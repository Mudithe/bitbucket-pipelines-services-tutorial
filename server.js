var express = require('express');
var mongoose = require('mongoose');

var app = express();

var Visit = require ('./visit');

mongoose.connection.on("error", function(err) {
  console.log("Could not connect to mongo server!");
  return console.log(err.message);
});


mongoose.connect('mongodb://localhost:27017/myappdatabase', { useNewUrlParser: true });

// The code below will display 'Hello World!' to the browser when you go to http://localhost:3000
app.get('/', function (req, res) {
  res.send('Hello World test!');
  var visit = new Visit();
  visit.save();
});

app.get('/visits', function (req, res) {
  Visit.find({}, function(err, visits){
    res.send(visits);
  })
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

module.exports = app;
